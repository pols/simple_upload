<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Post extends Controller {

	public function action_index(){

		$view = View::factory('common/template');
	 	$view->title = "Uploading App";
	 	$posts = ORM::factory('Post')->find_all();
	 	$view->body = View::factory('posts')->bind('posts', $posts);
	    // The view will have $places and $user variables
	    $this->response->body($view);

	}

} // End Welcome