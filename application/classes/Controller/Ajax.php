<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {

	public function action_index(){

		if( $this->request->is_ajax() ){
			$rdata = array();

			//var_dump($_FILES['InputFile']);
			//EDIT
			if( $this->request->post('task') == 'edit'){

				// UPLOAD FILE 
				$filename = $this->action_upload();
				//file passed
				$post = ORM::factory('post',$this->request->post('post_id'));
				$post->title = $this->request->post('title');
				$post->date_added = date('Y-m-d H:i:s');
				$post->filename = $filename;
				if( $post->save() ){
					$getData = ORM::factory('post', $this->request->post('post_id'));
					$arr = array('id' => $getData->id,'title' => $getData->title,'filename' => $getData->filename,'thumbnail' => $getData->thumbnail,'date_added' => $getData->date_added); //TODO expound data
					$rdata['tasks'] = "edit";
					$rdata['id'] = $this->request->post('post_id');
					$rdata['data'] = $arr;
				}
			}elseif( $this->request->post('task') == 'add' ){ // ADD
				// UPLOAD FILE 
				$filename = $this->action_upload();

				$post = ORM::factory('post');
				$post->date_added = date('Y-m-d H:i:s');
				$post->title = $this->request->post('title');
				$post->filename = $filename;
				if( $post->save() ){
					$getData = ORM::factory('post', $post->id);
					$arr = array('id' => $getData->id,'title' => $getData->title,'thumbnail' => !empty($getData->thumbnail)?$getData->thumbnail:"",'filename' => !empty($getData->filename)?$getData->filename:"",'date_added' => $getData->date_added); //TODO expound data
					$rdata['tasks'] = "add";
					$rdata['id'] = $post->id;
					$rdata['data'] = $arr;
				}

			}else{
				$post = ORM::factory('post', $this->request->post('id'));
				if( $post->delete() ){
					$rdata['tasks'] = "delete";
					$rdata['id'] = $this->request->post('id');
				}
			}

			echo json_encode($rdata);
				
		}
	}

	public function action_upload(){
        $error_message = NULL;
        $filename = NULL;
 
        if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['InputFile']))
            {
                return $filename = $this->_save_image($_FILES['InputFile']);
            }
        }
 
        if ( ! $filename)
        {
            $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
        }

 
    }

    protected function _save_image($image){
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
 
            Image::factory($file)
                ->resize(150, 150, Image::AUTO)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }


}