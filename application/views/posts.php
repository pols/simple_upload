<div class="col-md-12">

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Simple Uploading</h4>
	      </div>
	      <div class="modal-body">
	        ...
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary add-edit">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

	<table class="table table-striped">
  		<thead>
			<tr>
				<th>Thumbnail</th>
				<th>Title</th>
				<th>Filename</th>
				<th>Date Added</th>
				<th>Edit | Delete</th>
			</tr>
		</thead>
		<?php 
			if( count( $posts ) > 0 ){
			foreach( $posts as $post ): ?>
			<tr class="row-<?php echo $post->id; ?>">
				<td>
					<a href="#" class="thumbnail">
				      <img src="<?php echo 'uploads/'.$post->filename; ?>" class="img img-responsive" alt="...">
				    </a>
				</td>
				<td scope="row"><?php echo $post->title; ?></td>
				<td><?php echo $post->filename; ?></td>
				<td><?php echo $post->date_added; ?></td>
				<td>
				<a class="edit-thumbnail edit-thumbnail-<?php echo $post->id; ?>" href="javascript:void(0)" onclick="edit_thumbnail(<?php echo $post->id; ?>)" data-toggle="modal" rel="tooltip" data-original-title="Edit" data-id="<?php echo $post->id; ?>" data-text="<?php echo $post->title; ?>" data-notify="0">
					<span class="glyphicon glyphicon-edit"></span>
				</a> | 
				<a class="delete-thumbnail delete-thumbnail-<?php echo $post->id; ?>" onclick="delete_thumbnail(<?php echo $post->id; ?>)" data-original-title="Delete" data-id="<?php echo $post->id; ?>" data-notify="0">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
				</td>
			</tr>
		<?php 
			endforeach; 
			}else{
				echo '<tr class="no-data"><td colspan="5"><center>No data</center></td></tr>';
			}
			?>
	</table>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-add" data-toggle="modal" data-target="#myModal">
	  Add Thumbnail
	</button>
</div>
<script>

	function ajax(datas){
		// ajax call
		var n = $.ajax({
	      url: "ajax/index",
	      type: 'post',
	      data: datas,
	      dataType: 'json',
	      success: function(data, status) {
	        var ret = data;
	      },
	      async: false,
	      contentType: false, //must, tell jQuery not to process the data
          processData: false,
          mimeType: "multipart/form-data",
	      error: function(xhr, desc, err) {
	        console.log(xhr);
	        console.log("Details: " + desc + "\nError:" + err);
	      }
	    }); // end ajax call
	    return n.responseText;
	}

	function ajax2(datas){
		// ajax call
		var n = $.ajax({
	      url: "ajax/index",
	      type: 'post',
	      data: datas,
	      dataType: 'json',
	      success: function(data, status) {
	        var ret = data;
	      },
	      async: false,
	      error: function(xhr, desc, err) {
	        console.log(xhr);
	        console.log("Details: " + desc + "\nError:" + err);
	      }
	    }); // end ajax call
	    return n.responseText;
	}

	function edit_thumbnail(id){
		//e.preventDefault();
			$('#myModal').modal('show');
			var id = $('.edit-thumbnail-'+id).data('id');
			var text = $('.edit-thumbnail-'+id).data('text');
				var form = '<form id="upload_form" enctype="multipart/form-data">\
							  <div class="form-group">\
							    <label for="title">Title</label>\
							    <input type="text" class="form-control" name="title" id="title" value="'+text+'" placeholder="Title">\
							  </div>\
							  <div class="form-group">\
							    <label for="InputFile">File input</label>\
							    <input type="file" id="InputFile">\
							    <input type="hidden" name="task" id="task" class="task" value="edit" />\
							    <input type="hidden" name="post_id" id="post_id" class="post_id" value="'+id+'" />\
							  </div>\
							</form>\
							  ';		  

			$('.modal-body').html(form);
	}

	function delete_thumbnail(id){
		var r = confirm("Delete?");
			if (r == true) {
				var datas = { task: 'delete', id: $('.delete-thumbnail-'+id).data('id') }
				var ajx = jQuery.parseJSON(ajax2(datas));
				if( ajx.tasks == 'delete' ){
					$('.row-'+ajx.id).remove();
				}
			}
	}

	jQuery(document).ready(function($){

		// ADD AND EDIT
		$('.add-edit').on('click',function(e){
			e.preventDefault();

			if ($('.no-data').length > 0) {
				$('.no-data').remove();
			}

			var task = $('.task').val();

			var form = new FormData(document.getElementById('upload_form'));
			var file = document.getElementById('InputFile').files[0];
			if (file) {   
		        form.append('InputFile', file);
		    }

			if( task ){
				//var datas = {task: task, id: $('.post_id').val(), title: $('#title').val()};
				var datas = form;
			}

			var ajx = jQuery.parseJSON(ajax(datas));

			var url_t = top.location.href;
			var url = url_t.replace("\/#","");

			//console.log(ajx.data);
			if( ajx.tasks == 'edit'){

				var process = '<a class="edit-thumbnail edit-thumbnail-'+ajx.data.id+'" onclick="edit_thumbnail('+ajx.data.id+')"  data-toggle="modal" rel="tooltip" data-original-title="Edit" data-id="'+ajx.data.id+'" data-text="'+ajx.data.title+'" data-notify="0">\
									<span class="glyphicon glyphicon-edit"></span>\
								</a> | \
								<a class="delete-thumbnail delete-thumbnail-'+ajx.data.id+'" onclick="delete_thumbnail('+ajx.data.id+')"data-original-title="Delete" data-id="'+ajx.data.id+'" data-notify="0">\
									<span class="glyphicon glyphicon-trash"></span>\
								</a>';
				var thumbnail = '<a href="#" class="thumbnail">\
							      <img src="'+url+'/uploads/'+ajx.data.filename+'" class="img img-responsive" alt="...">\
							    </a>';

				var html = "<td>"+thumbnail+"</td>\
							<td>"+ajx.data.title+"</td>\
							<td>"+ajx.data.filename+"</td>\
							<td>"+ajx.data.date_added+"</td>\
							<td>"+process+"</td>";
				$('.row-'+ajx.id).html(html);
			}else{
				var process = '<a class="edit-thumbnail edit-thumbnail-'+ajx.data.id+'"  onclick="edit_thumbnail('+ajx.data.id+')" data-toggle="modal" rel="tooltip" data-original-title="Edit" data-id="'+ajx.data.id+'" data-text="'+ajx.data.title+'" data-notify="0">\
									<span class="glyphicon glyphicon-edit"></span>\
								</a> | \
								<a class="delete-thumbnail delete-thumbnail-'+ajx.data.id+'" onclick="delete_thumbnail('+ajx.data.id+')" data-original-title="Delete" data-id="'+ajx.data.id+'" data-notify="0">\
									<span class="glyphicon glyphicon-trash"></span>\
								</a>';
				var thumbnail = '<a href="#" class="thumbnail">\
							      <img src="'+url+'/uploads/'+ajx.data.filename+'" class="img img-responsive" alt="...">\
							    </a>';

				var html = "<tr class='row-"+ajx.data.id+"'>\
							<td>"+thumbnail+"</td>\
							<td>"+ajx.data.title+"</td>\
							<td>"+ajx.data.filename+"</td>\
							<td>"+ajx.data.date_added+"</td>\
							<td>"+process+"</td>\
							</tr>";
				$('.table-striped tbody').append(html);
			}
			jQuery("#myModal").modal('hide');
			return false;
		});


		// SAVE
		$('.btn-add').click(function(e){
			e.preventDefault();
			var form = '<form id="upload_form" enctype="multipart/form-data">\
						  <div class="form-group">\
						    <label for="title">Title</label>\
						    <input type="text" name="title" class="form-control" id="title" value="" placeholder="Title">\
						  </div>\
						  <div class="form-group">\
						    <label for="InputFile">File input</label>\
						    <input type="file" id="InputFile">\
						    <input type="hidden" name="task" id="task" class="task" value="add" />\
						  </div>\
						</form>\
							  ';	
			$('.modal-body').html(form);
			
		});
	});
</script>
<style>
	.thumbnail{ max-width: 150px; }
	.edit-thumbnail , .delete-thumbnail{ cursor:pointer;}
</style>